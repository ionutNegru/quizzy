package com.example.quizzy.adapters.models;

public class CategoriesDescriptions {

    private String category;
    private String description;

    public CategoriesDescriptions(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CategoriesDescriptions{" +
                "category='" + category + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
