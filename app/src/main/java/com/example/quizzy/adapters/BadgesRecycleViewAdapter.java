package com.example.quizzy.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import com.example.quizzy.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class BadgesRecycleViewAdapter extends RecyclerView.Adapter<BadgesRecycleViewAdapter.ViewHolder> implements Filterable {

    private LayoutInflater layoutInflater;

    private long[] noQCategories;
    private long[] noQAns;
    private ArrayList<String> categoriesNames;

    private ArrayList<Boolean> dummyBadges = new ArrayList<>();
    private ArrayList<Integer> dummyArray = new ArrayList<>();

    private ArrayList<Integer> originalArray = new ArrayList<>();

    private final Integer[] BADGES = {R.drawable.sport, R.drawable.corp,  R.drawable.jurul};
            //R.drawable.istorie, R.drawable.natura,  R.drawable.planeta,
            //R.drawable.tehn, R.drawable.universul};


    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            imageView = v.findViewById(R.id.profile_badge);
        }
    }


    public BadgesRecycleViewAdapter(Context context, long[] noQCategories, long[] noQAns, ArrayList<String> categoriesNames) {
        this.context = context;
        this.noQCategories = noQCategories;
        this.noQAns = noQAns;
        this.categoriesNames = categoriesNames;

        for (int k=0; k<noQCategories.length; k++){
            if (noQCategories[k] == noQAns[k]) {
                dummyBadges.add(k,true);
            } else {
                dummyBadges.add(k, false);
            }
        }

        dummyArray.addAll(Arrays.asList(BADGES));

        int k = 0;
        for(int i = 0; i< dummyBadges.size(); i++){
            if(!dummyBadges.get(i)){
                dummyArray.remove(i-k);
                categoriesNames.remove(i-k);
                k++;
            }
        }

        originalArray.addAll(dummyArray);

        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemCount() {
        return dummyArray.size();
    }

    @Override
    public BadgesRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = layoutInflater.inflate(R.layout.custom_badge,parent,false);

        BadgesRecycleViewAdapter.ViewHolder vh = new BadgesRecycleViewAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(BadgesRecycleViewAdapter.ViewHolder holder, final int position) {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), dummyArray.get(position));

            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 256, 256, true);
            holder.imageView.setImageBitmap(scaledBitmap);
    }

    @Override
    public Filter getFilter() {
        return filteredValues;
    }

    private Filter filteredValues = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Integer> filteredIntValues = new ArrayList<>();

            if (constraint == null || constraint.toString().isEmpty()) {
                filteredIntValues.addAll(originalArray);
            } else {
                for (int i = 0; i<categoriesNames.size(); i++) {
                    if (categoriesNames.get(i).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filteredIntValues.add(originalArray.get(i));
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredIntValues;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dummyArray.clear();
            dummyArray.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

}
