package com.example.quizzy.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.quizzy.R;
import com.example.quizzy.activities.SinglePlayer;
import com.example.quizzy.firebase.models.Category;

import java.util.ArrayList;
import java.util.Map;


public class CategoriesPageAdapter extends PagerAdapter {

    private final Integer[] IMAGINI = {R.drawable.sport, R.drawable.corp,  R.drawable.jurul,
                                        R.drawable.istorie, R.drawable.natura,  R.drawable.planeta,
                                        R.drawable.tehn, R.drawable.universul};
    private ArrayList<Category> categories;
    private ArrayList<Map<String,Object>> categoriesAnswered;

    private Context context;

    public CategoriesPageAdapter(Context context, ArrayList<Category> categories, ArrayList<Map<String,Object>> categoriesAnswered) {
        this.context = context;
        this.categories = categories;
        this.categoriesAnswered = categoriesAnswered;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {


        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.categories_slider,container,false);

        ImageView imageView = view.findViewById(R.id.categoryImage);
        imageView.setImageResource(IMAGINI[position]);

        TextView textView = view.findViewById(R.id.categoryTitle);
        textView.setText(categories.get(position).getCategoryName());

        TextView textView1 = view.findViewById(R.id.categoryDescription);
        textView1.setText(categories.get(position).getCategoryDescription());

        TextView textView2 = view.findViewById(R.id.sf_percentage);
        if (categoriesAnswered.get(position).get("noQuestionsAnswered") == categories.get(position).getNoQuestions()) {
            textView2.setText("");
            textView2.append("Complet");
            view.setEnabled(false);
        } else {
            textView2.setText("");
            double percentage = ((Long)categoriesAnswered.get(position).get("noQuestionsAnswered") * 100) / categories.get(position).getNoQuestions();
            textView2.append("(" + percentage + " % complet)");
        }

        ViewPager viewPager = (ViewPager)container;
        viewPager.addView(view, 0);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context,
                        SinglePlayer.class);

                Bundle bundle = new Bundle();
                bundle.putInt("index",position);
                bundle.putString("category", categories.get(position).getCategoryName());

                intent.putExtras(bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent);

            }
        });

        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager viewPager = (ViewPager)container;
        View view = (View) object;
        viewPager.removeView(view);
    }

}
