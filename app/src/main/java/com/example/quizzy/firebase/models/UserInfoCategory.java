package com.example.quizzy.firebase.models;

public class UserInfoCategory {
    private String name;
    private Long noQuestionsAnswered;
    private Long totalQuestionsAnswered;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNoQuestionsAnswered() {
        return noQuestionsAnswered;
    }

    public void setNoQuestionsAnswered(Long noQuestionsAnswered) {
        this.noQuestionsAnswered = noQuestionsAnswered;
    }

    public Long getTotalQuestionsAnswered() {
        return totalQuestionsAnswered;
    }

    public void setTotalQuestionsAnswered(Long totalQuestionsAnswered) {
        this.totalQuestionsAnswered = totalQuestionsAnswered;
    }
}
