package com.example.quizzy.firebase.models;

public class Category {

    private String categoryName;
    private String categoryDescription;
    private Long noQuestions;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public Long getNoQuestions() {
        return noQuestions;
    }

    public void setNoQuestions(Long noQuestions) {
        this.noQuestions = noQuestions;
    }
}
