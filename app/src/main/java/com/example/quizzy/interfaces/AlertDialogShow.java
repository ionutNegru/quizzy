package com.example.quizzy.interfaces;

public interface AlertDialogShow {

    void setUpAlertDialog(String alert);

    void setUpWarningDialog(String warning);
}
