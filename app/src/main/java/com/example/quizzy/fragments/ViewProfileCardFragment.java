package com.example.quizzy.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.quizzy.R;
import com.example.quizzy.adapters.BadgesRecycleViewAdapter;
import com.example.quizzy.helper.AvatarSetter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;


public class ViewProfileCardFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    private View view;

    private ImageView card_avatar;
    private TextView card_username;
    private TextView card_answers;
    private TextView card_percentage;
    private BadgesRecycleViewAdapter badgesRecycleViewAdapter;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView badgesList;
    private SearchView searchView;
    private View relative_pf;
    private ProgressBar progressBar;

    private Bundle bundle;

    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser user;

    private Bitmap avatar;

    private OnFragmentInteractionListener mListener;


    public ViewProfileCardFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_view_profile_card, container, false);

        bindElements();

        bundle = this.getArguments();

        mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        badgesList.setLayoutManager(mLayoutManager);

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        relative_pf.setVisibility(View.INVISIBLE);
        relative_pf.setEnabled(false);

        String path = user.getUid().concat(".jpg");

        storageReference.child("images")
                .child(path)
                .getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        if (uri != null) {
                            Glide.with(getContext())
                                    .asBitmap()
                                    .load(uri)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            avatar = resource;

                                            progressBar.setVisibility(View.INVISIBLE);
                                            relative_pf.setEnabled(true);
                                            relative_pf.setVisibility(View.VISIBLE);

                                            setupCards();
                                        }
                                    });
                        }
                    }
                });

        return view;
    }

    public void setupCards(){

        card_username.setText(user.getDisplayName());

        long categoriesAns = bundle.getLong("noQuestionsAnswered");
        long categoriesTotal = bundle.getLong("totalQuestionsAnswered");

        if(categoriesTotal == 0){
            card_answers.setText(getResources().getString(R.string.warning_statistics));
            card_percentage.setText(getResources().getString(R.string.warning_statistics));
        }
        else
        {
            card_answers.setText(categoriesAns + " din " + categoriesTotal);

            double percentage = (categoriesAns * 100)/ categoriesTotal;
            card_percentage.setText(percentage + "%");
        }

        card_avatar.setImageBitmap(AvatarSetter.setUpAvatar(avatar,256, 256));

        long[] noQCategories = bundle.getLongArray("noQuestionsCategories");
        long[] noQAns = bundle.getLongArray("noQuestionsAns");
        ArrayList<String> categoriesNames = bundle.getStringArrayList("categoriesNames");
        badgesRecycleViewAdapter = new BadgesRecycleViewAdapter(getContext(), noQCategories, noQAns, categoriesNames);
        badgesList.setAdapter(badgesRecycleViewAdapter);

        setUpSearchView();
    }

    public void setUpSearchView(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                badgesRecycleViewAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void bindElements(){
        progressBar = view.findViewById(R.id.pf_progress);
        relative_pf = view.findViewById(R.id.relative_pf);
        card_avatar = view.findViewById(R.id.card_avatar);
        card_username = view.findViewById(R.id.card_username);
        card_answers = view.findViewById(R.id.card_second_right);
        card_percentage = view.findViewById(R.id.card_second_percentage);
        searchView = view.findViewById(R.id. categories_search);
        badgesList  = view.findViewById(R.id.badges_recycler);
    }
}
