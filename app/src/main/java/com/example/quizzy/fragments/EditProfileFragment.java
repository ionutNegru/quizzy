package com.example.quizzy.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.quizzy.R;
import com.example.quizzy.helper.AvatarSetter;
import com.example.quizzy.helper.FormValidator;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;


public class EditProfileFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private FirebaseAuth firebaseAuth;
    private View view;

    private EditText nameText;
    private Button epfButton;
    private Button revertButton;

    private View epfRelative;
    private ProgressBar epfProgress;
    private ImageView epAvatar;

    private int GALLERY = 1, CAMERA = 2;

    private FirebaseFirestore firebaseFirestore;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;

    private Bitmap selectedImage;

    private FirebaseUser user;

    private Bitmap avatar;

    private static final int MY_CAMERA_REQUEST_CODE = 100;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        bindElements();

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();

        nameText.setHint(user.getDisplayName());

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        epfRelative.setVisibility(View.INVISIBLE);
        epfRelative.setEnabled(false);

        String path = user.getUid().concat(".jpg");

        storageReference.child("images")
                .child(path)
                .getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        if (uri != null) {
                            Glide.with(getContext())
                                    .asBitmap()
                                    .load(uri)
                                    .into(new SimpleTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            avatar = resource;
                                            epAvatar.setImageBitmap(AvatarSetter.setUpAvatar(resource,512, 512));

                                            epfProgress.setVisibility(View.INVISIBLE);
                                            epfRelative.setEnabled(true);
                                            epfRelative.setVisibility(View.VISIBLE);
                                        }
                                    });
                        }
                    }
                });

        epfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                epfProgress.setVisibility(View.VISIBLE);
                epfRelative.setEnabled(false);
                epfRelative.setVisibility(View.INVISIBLE);
                attemptUpdate();
            }
        });

        revertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameText.setText("");
                nameText.setHint(user.getDisplayName());
                epAvatar.setImageBitmap(AvatarSetter.setUpAvatar(avatar,512, 512));
            }
        });

        epAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();
            }
        });

        return view;
    }

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Photo Gallery",
                "Camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallery();
                                break;
                            case 1:
                                checkPermissionFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Bundle bundle = new Bundle();
        this.setArguments(bundle);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(intent, CAMERA);
    }

    private void checkPermissionFromCamera() {

        if (getContext().checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_REQUEST_CODE);
        } else {
            takePhotoFromCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhotoFromCamera();
            } else {
                Toast.makeText(getContext(), "Camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri imageUri = data.getData();
                try {
                    selectedImage = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imageUri);

                    Bitmap fitBitmap = Bitmap.createScaledBitmap(selectedImage, 512, 512,true);
                    Bitmap croppedBitmap = AvatarSetter.getCroppedBitmap(fitBitmap);

                    epAvatar.setImageBitmap(croppedBitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else if (requestCode == CAMERA) {
            if (data != null) {
                selectedImage = (Bitmap)data.getExtras().get("data");

                Bitmap fitBitmap = Bitmap.createScaledBitmap(selectedImage, 512, 512,true);
                Bitmap croppedBitmap = AvatarSetter.getCroppedBitmap(fitBitmap);

                epAvatar.setImageBitmap(croppedBitmap);
            }
        }
    }

    private void attemptUpdate() {
        nameText.setError(null);

        String displayName = nameText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(displayName) && selectedImage == null) {
            cancel = true;
            focusView = nameText;
            Toast.makeText(getContext(), "Nu ai modificat nimic", Toast.LENGTH_LONG).show();
        }

        if(!TextUtils.isEmpty(displayName) && !FormValidator.isUserValid(displayName)){
            nameText.setError(getString(R.string.error_invalid_username));
            focusView = nameText;
            cancel = true;
        }

        if (cancel) {
            epfProgress.setVisibility(View.INVISIBLE);
            epfRelative.setEnabled(true);
            epfRelative.setVisibility(View.VISIBLE);

            focusView.requestFocus();
        } else {
                if(TextUtils.isEmpty(displayName)){
                    displayName = user.getDisplayName();
                }
                updateUser(displayName);
        }
    }

    private void updateUser(final String displayName) {

        UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(displayName).build();

        user.updateProfile(userProfileChangeRequest).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                HashMap<String, String> data = new HashMap<>();
                data.put("displayName",displayName);
                data.put("email",user.getEmail());
                firebaseFirestore = FirebaseFirestore.getInstance();
                firebaseFirestore.collection("UserInfo").document(user.getUid()).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if (selectedImage != null) {
                            uploadImage(selectedImage);
                        } else {
                            epfProgress.setVisibility(View.INVISIBLE);
                            epfRelative.setEnabled(true);
                            epfRelative.setVisibility(View.VISIBLE);
                            nameText.setText("");
                            nameText.setHint(user.getDisplayName());
                        }
                    }
                });
            }
        });
    }

    public void uploadImage(final Bitmap bitmap){
        final String path = user.getUid().concat(".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        final byte[] data = baos.toByteArray();

        storageReference.child("images")
                .child(path)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        UploadTask uploadTask = storageReference.child("images").child(path).putBytes(data);
                        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                epfProgress.setVisibility(View.INVISIBLE);
                                nameText.setHint(user.getDisplayName());
                                epfRelative.setEnabled(true);
                                epfRelative.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                });
    }

    public void bindElements(){
        nameText = view.findViewById(R.id.edit_Name);
        epfButton               = view.findViewById(R.id.epf_button_save);
        revertButton            = view.findViewById(R.id.epf_button_revert);
        epfRelative             = view.findViewById(R.id.relative_epf);
        epfProgress             = view.findViewById(R.id.epf_progress);
        epAvatar                = view.findViewById(R.id.epf_avatar);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
}
