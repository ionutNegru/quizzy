package com.example.quizzy.fragments;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quizzy.R;
import com.example.quizzy.adapters.SectionsPageAdapter;

import org.jetbrains.annotations.NotNull;


public class ProfileFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private View view;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Bundle bundle;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_profile, container, false);

        tabLayout =  view.findViewById(R.id.tabLayout);
        viewPager =  view.findViewById(R.id.viewpagerProfile);

        bundle = getArguments();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setupViewPager(@NotNull ViewPager viewPager) {
        SectionsPageAdapter sectionsPageAdapter = new SectionsPageAdapter(getChildFragmentManager());

        ViewProfileCardFragment viewFragment = new ViewProfileCardFragment();
        viewFragment.setArguments(bundle);

        EditProfileFragment editProfileFragment = new EditProfileFragment();

        sectionsPageAdapter.addFragment(editProfileFragment, "Editeaza profilul");
        sectionsPageAdapter.addFragment(viewFragment, "Vezi profilul");
        viewPager.setAdapter(sectionsPageAdapter);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
