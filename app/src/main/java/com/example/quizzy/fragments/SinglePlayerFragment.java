package com.example.quizzy.fragments;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.quizzy.R;
import com.example.quizzy.adapters.CategoriesPageAdapter;
import com.example.quizzy.firebase.models.Category;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

public class SinglePlayerFragment extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    private View view;
    private ViewPager categoriesViewPager;
    private View relative_spf;
    private ProgressBar progressBar;

    private ArrayList<Category> categories;
    private ArrayList<Map<String, Object>> categoriesAnswered;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;


    public SinglePlayerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_single_player, container, false);

        bindElements();

        categories = new ArrayList<>();
        categoriesAnswered = new ArrayList<>();

        relative_spf.setEnabled(false);
        relative_spf.setVisibility(View.INVISIBLE);

        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection("Categories").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots){
                    Map<String, Object> data = queryDocumentSnapshot.getData();

                    Category category = new Category();
                    category.setCategoryName((String)data.get("Name"));
                    category.setCategoryDescription((String)data.get("Description"));
                    category.setNoQuestions((Long)data.get("noQuestions"));

                    categories.add(category);
                }

                firebaseAuth = FirebaseAuth.getInstance();
                firebaseFirestore.collection("UserInfo").document(firebaseAuth.getCurrentUser().getUid())
                        .collection("Categories").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                            Map<String, Object> data = queryDocumentSnapshot.getData();
                            categoriesAnswered.add(data);
                        }

                        setupCategoriesViewPager(categoriesViewPager);
                        progressBar.setVisibility(View.INVISIBLE);
                        relative_spf.setVisibility(View.VISIBLE);
                        relative_spf.setEnabled(true);
                    }
                });
            }
        });

        return view;
    }

    public void setupCategoriesViewPager(ViewPager viewPager) {
        CategoriesPageAdapter categoriesPageAdapter = new CategoriesPageAdapter(getContext(), categories, categoriesAnswered);
        viewPager.setAdapter(categoriesPageAdapter);
    }

    public void bindElements(){
        categoriesViewPager =  view.findViewById(R.id.categoriesViewPager);
        relative_spf = view.findViewById(R.id.relative_spf);
        progressBar = view.findViewById(R.id.progress_bar_spf);
    }

}
