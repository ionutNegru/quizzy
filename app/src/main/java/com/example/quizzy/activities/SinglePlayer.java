package com.example.quizzy.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.quizzy.R;
import com.example.quizzy.firebase.models.Question;
import com.example.quizzy.firebase.models.UserInfoCategory;
import com.example.quizzy.interfaces.AlertDialogShow;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

public class SinglePlayer extends AppCompatActivity implements AlertDialogShow {

    private TextView question;
    private Button shareSp;
    private Button answer;
    private Button answer2;
    private Button answer3;
    private Button answer4;
    private Button backUser;
    private ProgressBar progressBar;
    private View relativeSp;
    private ImageView viata1;
    private ImageView viata2;
    private ImageView viata3;
    private TextView category;
    private static final String FORMAT = "00:%02d";
    private long millisToResume = 45000;
    private TextView timer;
    private boolean isPaused = false;
    private static String right_answer;
    private static String categorie;
    private int indexCategory;
    private ArrayList<Question> questionsList = new ArrayList<>();
    private Long indexQuestion = 1L;
    private Long totalCategory = 1L;
    private int localIndex = 0;
    private int totalQuestions = 0;
    private int totalCorrect = 0;
    private boolean firstUpdate = true;
    private static int nr_vieti = 3;
    private FirebaseUser user;
    private FirebaseFirestore firebaseFirestore;
    private CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player);

        bindElements();

        categorie = getIntent().getStringExtra("category");
        indexCategory = getIntent().getIntExtra("index",0);

        category.setText(categorie);

        backUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isPaused = true;
                setUpAlertDialog((getResources().getString(R.string.alert_exit)));

            }
        });

        relativeSp.setEnabled(false);
        relativeSp.setVisibility(View.INVISIBLE);

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();

        firebaseFirestore.collection("UserInfo").document(user.getUid())
                .collection("Categories").document(String.valueOf(indexCategory + 1)).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                indexQuestion = (Long)documentSnapshot.get("noQuestionsAnswered");
                Long dummy = (Long)documentSnapshot.get("totalQuestionsAnswered");
                totalQuestions = dummy.intValue();
                totalCorrect = indexQuestion.intValue();

                firebaseFirestore.collection("Categories").document(String.valueOf(indexCategory + 1))
                        .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                totalCategory = (Long)documentSnapshot.get("noQuestions");

                                firebaseFirestore.collection("Categories").document(String.valueOf(indexCategory + 1))
                                        .collection("Questions").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                        for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                                            Long id = Long.parseLong(queryDocumentSnapshot.getId());
                                            if (indexQuestion < id) {
                                                Map<String, Object> data = queryDocumentSnapshot.getData();

                                                Question question = new Question();
                                                question.setQuestion((String)data.get("question"));
                                                question.setAnswer1((String)data.get("answer1"));
                                                question.setAnswer2((String)data.get("answer2"));
                                                question.setAnswer3((String)data.get("answer3"));
                                                question.setAnswer4((String)data.get("answer4"));
                                                question.setCorrectAnswer((String)data.get("correctAnswer"));

                                                questionsList.add(question);
                                            }
                                        }
                                        if (firstUpdate) {
                                            setUpShareButton();
                                            shuffleList();
                                            setUpQuestion();
                                            setUpButtons();
                                        }
                                    }
                                        });
                            }
                        });
            }
                });
    }

    public void shuffleList() {
        if (questionsList.size() == 10) {
            Question question = questionsList.get(1);
            questionsList.remove(1);
            questionsList.add(question);
        } else {
            Question question = questionsList.get(0);
            questionsList.remove(0);
            questionsList.add(question);
        }
    }

    public void setUpQuestion(){
        if (localIndex == questionsList.size()) {
            isPaused = true;
            setUpWarningDialog(getResources().getString(R.string.warning_category));
        } else {
            if (!firstUpdate) {
                question.setText(questionsList.get(localIndex).getQuestion());
                answer.setText(questionsList.get(localIndex).getAnswer1());
                answer2.setText(questionsList.get(localIndex).getAnswer2());
                answer3.setText(questionsList.get(localIndex).getAnswer3());
                answer4.setText(questionsList.get(localIndex).getAnswer4());

                clearColors();
            } else {
                firstUpdate = false;

                question.setText(questionsList.get(localIndex).getQuestion());
                answer.setText(questionsList.get(localIndex).getAnswer1());
                answer2.setText(questionsList.get(localIndex).getAnswer2());
                answer3.setText(questionsList.get(localIndex).getAnswer3());
                answer4.setText(questionsList.get(localIndex).getAnswer4());

                progressBar.setVisibility(View.INVISIBLE);
                relativeSp.setVisibility(View.VISIBLE);
                relativeSp.setEnabled(true);
            }

            right_answer = questionsList.get(localIndex).getCorrectAnswer();

            if (countDownTimer != null) {
                countDownTimer.cancel();
                millisToResume = 45000;
            }

            startTimer();
        }
    }

    public void setUpShareButton(){
        shareSp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, questionsList.get(localIndex).getQuestion());
                sendIntent.setType("text/plain");

                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
            }
        });
    }

    public void setUpButtons(){
        answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalQuestions ++;
                if(right_answer.equals("1")){
                    totalCorrect++;
                    answer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));

                    localIndex++;

                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            setUpQuestion();
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
                else{
                    answer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));

                    nr_vieti--;

                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if(nr_vieti == 0) {
                                isPaused = true;
                                viata1.setVisibility(View.INVISIBLE);
                                setUpAlertDialog(getResources().getString(R.string.alert_loss));
                            } else {
                                if (nr_vieti == 2) {
                                    viata3.setVisibility(View.INVISIBLE);

                                } else if (nr_vieti == 1) {
                                    viata2.setVisibility(View.INVISIBLE);

                                }
                                clearColors();
                            }
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
            }
        });

        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalQuestions ++;
                if(right_answer.equals("2")){
                    totalCorrect++;
                    answer2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));

                    localIndex++;

                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            setUpQuestion();
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
                else {
                    answer2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));

                    nr_vieti--;
                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if(nr_vieti == 0) {
                                isPaused = true;
                                viata1.setVisibility(View.INVISIBLE);
                                setUpAlertDialog(getResources().getString(R.string.alert_loss));
                            } else {
                                if (nr_vieti == 2) {
                                    viata3.setVisibility(View.INVISIBLE);

                                } else if (nr_vieti == 1) {
                                    viata2.setVisibility(View.INVISIBLE);
                                }
                                clearColors();
                            }
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
            }
        });

        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalQuestions ++;
                if (right_answer.equals("3")) {
                    totalCorrect++;
                    answer3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));

                    localIndex++;

                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            setUpQuestion();
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                } else {
                    answer3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));

                    nr_vieti--;
                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if(nr_vieti == 0) {
                                isPaused = true;
                                viata1.setVisibility(View.INVISIBLE);
                                setUpAlertDialog(getResources().getString(R.string.alert_loss));
                            } else {
                                if (nr_vieti == 2) {
                                    viata3.setVisibility(View.INVISIBLE);

                                } else if (nr_vieti == 1) {
                                    viata2.setVisibility(View.INVISIBLE);

                                }
                                clearColors();
                            }
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
            }
        });

        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalQuestions ++;
                if(right_answer.equals("4")){
                    totalCorrect++;
                    answer4.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));

                    localIndex++;

                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            setUpQuestion();
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
                else{
                    answer4.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));

                    nr_vieti--;

                    Handler mHandler = new Handler();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            if(nr_vieti == 0) {
                                isPaused = true;
                                viata1.setVisibility(View.INVISIBLE);
                                setUpAlertDialog(getResources().getString(R.string.alert_loss));
                            } else {
                                if (nr_vieti == 2) {
                                    viata3.setVisibility(View.INVISIBLE);

                                } else if (nr_vieti == 1) {
                                    viata2.setVisibility(View.INVISIBLE);

                                }
                                clearColors();
                            }
                        }
                    };
                    mHandler.postDelayed(runnable, 1500);
                }
            }
        });
    }

    public void startTimer(){

        Log.e("timer","inceput");
        Log.e("milies",String.valueOf(millisToResume));

        countDownTimer = new CountDownTimer(millisToResume, 1000) {
            public void onTick(long millisUntilFinished) {

                if (isPaused) {
                    cancel();
                } else {
                    timer.setText("" + String.format(FORMAT,
                            millisUntilFinished / 1000));
                    millisToResume = millisUntilFinished;
                }

            }

            public void onFinish() {
                Log.e("timer", "terminat");
                setUpAlertDialog(getResources().getString(R.string.alert_time));
            }
        }.start();
    }

    @Override
    public void setUpWarningDialog(String warning) {

        AlertDialog.Builder builder = new AlertDialog.Builder(SinglePlayer.this).setCancelable(false);
        View view1 = getLayoutInflater().inflate(R.layout.warning_dialog, null);


        builder.setView(view1);
        final AlertDialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if(!isFinishing())
        {
            dialog.show();
        }

        TextView textAlert = dialog.findViewById(R.id.warning_titlu);
        textAlert.setText(warning);

        Button okButton = dialog.findViewById(R.id.ok_button);

        okButton.setText(getResources().getString(R.string.warning_ok));

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserInfoCategory data = new UserInfoCategory();
                data.setName(categorie);
                data.setNoQuestionsAnswered(totalCategory);
                data.setTotalQuestionsAnswered((long)totalQuestions);
                firebaseFirestore.collection("UserInfo").document(user.getUid())
                        .collection("Categories").document(String.valueOf(indexCategory + 1)).set(data)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                dialog.dismiss();

                                Intent intent = new Intent(SinglePlayer.this,
                                        HomeScreen.class);
                                startActivity(intent);
                                finish();
                            }
                        });
            }
        });
    }

    public void setUpAlertDialog(String alert){

        AlertDialog.Builder builder = new AlertDialog.Builder(SinglePlayer.this).setCancelable(false);
        View view1 = getLayoutInflater().inflate(R.layout.alert_dialog, null);

        builder.setView(view1);
        final AlertDialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if(!isFinishing())
        {
            dialog.show();
        }

        TextView textAlert = dialog.findViewById(R.id.dialog_titlu);
        textAlert.setText(alert);

        Button affirmativeAnswer = dialog.findViewById(R.id.affirmative_button);
        Button negativeAnswer = dialog.findViewById(R.id.negative_button);

        if(alert.equals(getResources().getString(R.string.alert_exit))){

            affirmativeAnswer.setText(getResources().getString(R.string.alert_exit_yes));
            negativeAnswer.setText(getResources().getString(R.string.alert_exit_no));

            affirmativeAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    UserInfoCategory data = new UserInfoCategory();
                    data.setName(categorie);
                    data.setNoQuestionsAnswered((long)totalCorrect);
                    data.setTotalQuestionsAnswered((long)totalQuestions);
                    firebaseFirestore.collection("UserInfo").document(user.getUid())
                            .collection("Categories").document(String.valueOf(indexCategory + 1)).set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    dialog.dismiss();

                                    nr_vieti = 3;
                                    Intent intent = new Intent(SinglePlayer.this,
                                            HomeScreen.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                }
            });

            negativeAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isPaused = false;
                    startTimer();

                    dialog.dismiss();
                }
            });
        } else {

            affirmativeAnswer.setText(getResources().getString(R.string.alert_loss_yes));
            negativeAnswer.setText(getResources().getString(R.string.alert_loss_no));


            affirmativeAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    UserInfoCategory data = new UserInfoCategory();
                    data.setName(categorie);
                    data.setNoQuestionsAnswered((long)totalCorrect);
                    data.setTotalQuestionsAnswered((long)totalQuestions);
                    firebaseFirestore.collection("UserInfo").document(user.getUid())
                            .collection("Categories").document(String.valueOf(indexCategory + 1)).set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    millisToResume = 46000;

                                    nr_vieti = 3;


                                    viata1.setVisibility(View.VISIBLE);
                                    viata2.setVisibility(View.VISIBLE);
                                    viata3.setVisibility(View.VISIBLE);

                                    progressBar.setVisibility(View.VISIBLE);
                                    relativeSp.setVisibility(View.INVISIBLE);
                                    relativeSp.setEnabled(false);

                                    isPaused = false;
                                    firstUpdate = true;
                                    clearColors();

                                    dialog.dismiss();

                                    Handler mHandler = new Handler();
                                    Runnable runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            setUpQuestion();
                                        }
                                    };
                                    mHandler.postDelayed(runnable, 1500);
                                }
                            });
                }
            });

            negativeAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    UserInfoCategory data = new UserInfoCategory();
                    data.setName(categorie);
                    data.setNoQuestionsAnswered((long)totalCorrect);
                    data.setTotalQuestionsAnswered((long)totalQuestions);
                    firebaseFirestore.collection("UserInfo").document(user.getUid())
                            .collection("Categories").document(String.valueOf(indexCategory + 1)).set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    dialog.dismiss();

                                    nr_vieti = 3;

                                    Intent intent = new Intent(SinglePlayer.this,
                                            HomeScreen.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                }
            });
        }
    }

    public void clearColors(){
        answer.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorBrown));
        answer2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorBrown));
        answer3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorBrown));
        answer4.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorBrown));
    }

    public void bindElements(){
        progressBar = findViewById(R.id.progress_bar_sp);
        relativeSp = findViewById(R.id.single_player_view);
        shareSp = findViewById(R.id.sp_share);

        question = findViewById(R.id.spf_intrebare);
        category = findViewById(R.id.spf_categorieTitlu);
        answer   = findViewById(R.id.spf_raspuns);
        answer2  = findViewById(R.id.spf_raspuns2);
        answer3  = findViewById(R.id.spf_raspuns3);
        answer4  = findViewById(R.id.spf_raspuns4);
        backUser = findViewById(R.id.userBack);

        timer    = findViewById(R.id.spf_timer);
        viata1   = findViewById(R.id.userBarHeart);
        viata2   = findViewById(R.id.userBarHeart1);
        viata3   = findViewById(R.id.userBarHeart2);
    }
}
