package com.example.quizzy.activities;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.quizzy.R;
import com.example.quizzy.helper.FormValidator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.HashMap;

public class LoginUser extends AppCompatActivity {

    private View relativeLogin;
    private ProgressBar progressBar;
    private EditText loginEmailView;
    private EditText loginPasswordView;
    private Button loginButton;
    private LoginButton loginButtonFB;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;
    private CallbackManager mCallbackManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);

        bindElements();

        firebaseAuth = FirebaseAuth.getInstance();

        loginPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    relativeLogin.setVisibility(View.INVISIBLE);
                    relativeLogin.setEnabled(false);
                    progressBar.setVisibility(View.VISIBLE);
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });


        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


        loginButtonFB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButtonFB.setReadPermissions("email", "public_profile");
                relativeLogin.setVisibility(View.INVISIBLE);
                relativeLogin.setEnabled(false);
                progressBar.setVisibility(View.VISIBLE);

                mCallbackManager = CallbackManager.Factory.create();
                loginButtonFB.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        relativeLogin.setVisibility(View.VISIBLE);
                        relativeLogin.setEnabled(true);
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError(FacebookException error) {
                        relativeLogin.setVisibility(View.VISIBLE);
                        relativeLogin.setEnabled(true);
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(LoginUser.this, error.toString(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();

        if(currentUser != null) {
            signOut();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(@NonNull AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            final FirebaseUser user = firebaseAuth.getCurrentUser();

                            final boolean[] addUser = {true};
                            firebaseFirestore = FirebaseFirestore.getInstance();
                            firebaseFirestore.collection("UserInfo").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    if (queryDocumentSnapshots != null) {
                                        for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                                            if (user.getUid().equals(queryDocumentSnapshot.getId())) {
                                                addUser[0] = false;
                                            }
                                        }
                                        if (addUser[0]) {
                                            HashMap<String, String> data = new HashMap<>();
                                            data.put("displayName",user.getDisplayName());
                                            data.put("email",user.getEmail());
                                            firebaseFirestore.collection("UserInfo").document(user.getUid()).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Intent intent = new Intent(LoginUser.this,
                                                            HomeScreen.class);
                                                    startActivity(intent);
                                                }
                                            });
                                        } else {
                                            //profile = Profile.getCurrentProfile();
                                            Intent intent = new Intent(LoginUser.this,
                                                    HomeScreen.class);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            });

                        } else {
                            Toast.makeText(LoginUser.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void signOut() {
        firebaseAuth.signOut();
        LoginManager.getInstance().logOut();
    }

    private void attemptLogin() {

        loginEmailView.setError(null);
        loginPasswordView.setError(null);

        String email = loginEmailView.getText().toString();
        String password = loginPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(password)) {
            loginPasswordView.setError(getString(R.string.error_field_required));
            focusView = loginPasswordView;
            cancel = true;
        } else if(!FormValidator.isPasswordValid(password)) {
            loginPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = loginPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            loginEmailView.setError(getString(R.string.error_field_required));
            focusView = loginEmailView;
            cancel = true;
        } else if (!FormValidator.isUserValid(email)) {
            loginEmailView.setError(getString(R.string.error_invalid_email));
            focusView = loginEmailView;
            cancel = true;
        }

        if (cancel) {
            relativeLogin.setVisibility(View.VISIBLE);
            relativeLogin.setEnabled(true);
            progressBar.setVisibility(View.INVISIBLE);
            focusView.requestFocus();
        } else {

            firebaseAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                final FirebaseUser user = firebaseAuth.getCurrentUser(); //ionutTest

                                final boolean[] addUser = {true};
                                firebaseFirestore = FirebaseFirestore.getInstance();
                                firebaseFirestore.collection("UserInfo").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                            if (queryDocumentSnapshots != null) {
                                                for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                                                    if (user.getUid().equals(queryDocumentSnapshot.getId())) {
                                                        addUser[0] = false;
                                                    }
                                                }
                                                if (addUser[0]) {
                                                    HashMap<String, String> data = new HashMap<>();
                                                    data.put("displayName",user.getEmail().split("@")[0]);
                                                    data.put("email",user.getEmail());
                                                    firebaseFirestore.collection("UserInfo").document(user.getUid()).set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Intent intent = new Intent(LoginUser.this,
                                                                    HomeScreen.class);
                                                            startActivity(intent);
                                                        }
                                                    });
                                                } else {
                                                    Intent intent = new Intent(LoginUser.this,
                                                            HomeScreen.class);
                                                    startActivity(intent);
                                                }
                                            }
                                    }
                                });
                            } else {
                                Toast.makeText(LoginUser.this, getString(R.string.error_invalid_username_password),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    public void bindElements() {
        relativeLogin           =  findViewById(R.id.relative_login);
        progressBar             =  findViewById(R.id.progress_bar_login);
        loginEmailView          =  findViewById(R.id.login_email);
        loginPasswordView       =  findViewById(R.id.login_password);
        loginButton             =  findViewById(R.id.intra_Cont);
        loginButtonFB           =  findViewById(R.id.loginFB);
    }

}

