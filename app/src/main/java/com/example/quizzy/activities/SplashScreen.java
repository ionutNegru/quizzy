package com.example.quizzy.activities;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.motion.widget.TransitionAdapter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.example.quizzy.R;

public class SplashScreen extends AppCompatActivity {

    private ImageView splash_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splash_logo = findViewById(R.id.splash_logo);
        MotionLayout motionLayout = findViewById(R.id.motion_start);

        animateLogo(splash_logo);

        motionLayout.setTransitionListener(new TransitionAdapter() {
            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int currentId) {
                super.onTransitionCompleted(motionLayout, currentId);

                Intent intent = new Intent(SplashScreen.this, LoginUser.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void animateLogo (View view){
        ObjectAnimator logoAnimation = ObjectAnimator.ofFloat(splash_logo, "rotation", 0f, 360f);
        logoAnimation.setDuration(5000);
        logoAnimation.setRepeatMode(ValueAnimator.REVERSE);
        logoAnimation.setRepeatCount(ValueAnimator.INFINITE);
        logoAnimation.start();
    }

}
