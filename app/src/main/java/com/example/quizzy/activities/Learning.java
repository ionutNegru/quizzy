package com.example.quizzy.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.quizzy.R;
import com.example.quizzy.interfaces.AlertDialogShow;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import java.util.Arrays;
import java.util.List;

public class Learning extends YouTubeBaseActivity implements AlertDialogShow {


    private Button backUser;
    private YouTubePlayerView youTubePlayer;
    private List<String> videosList = Arrays.asList("pdi6o5popkU","bkWHrWw5yTg");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning);

        bindElements();

        backUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpAlertDialog((getResources().getString(R.string.alert_exit)));
            }
        });

        YouTubePlayer.OnInitializedListener onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.cueVideos(videosList);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Log.d(Learning.class.toString(), "Failed to initialize");
            }
        };

        String androidKey = "AIzaSyDutHyJTc0fxfYYjVeXCWRcp4Toz5SQ_Dk";
        youTubePlayer.initialize(androidKey, onInitializedListener);
    }

    public void setUpAlertDialog(String alert){
        AlertDialog.Builder builder = new AlertDialog.Builder(Learning.this).setCancelable(false);
        View view1 = getLayoutInflater().inflate(R.layout.alert_dialog, null);

        builder.setView(view1);
        final AlertDialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if(!isFinishing())
        {
            dialog.show();
        }

        TextView textAlert = dialog.findViewById(R.id.dialog_titlu);
        textAlert.setText(alert);

        Button affirmativeAnswer = dialog.findViewById(R.id.affirmative_button);
        Button negativeAnswer = dialog.findViewById(R.id.negative_button);


        affirmativeAnswer.setText(getResources().getString(R.string.alert_exit_yes));
        negativeAnswer.setText(getResources().getString(R.string.alert_exit_no));

        affirmativeAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent intent = new Intent(Learning.this,
                        HomeScreen.class);
                startActivity(intent);
                finish();

            }
        });

        negativeAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


    }

    @Override
    public void setUpWarningDialog(String warning) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Learning.this).setCancelable(false);
        View view1 = getLayoutInflater().inflate(R.layout.warning_dialog, null);


        builder.setView(view1);
        final AlertDialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if(!isFinishing())
        {
            dialog.show();
        }

        TextView textAlert = dialog.findViewById(R.id.warning_titlu);
        textAlert.setText(warning);

        Button okButton = dialog.findViewById(R.id.ok_button);

        okButton.setText(getResources().getString(R.string.warning_ok));

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent intent = new Intent(Learning.this,
                        HomeScreen.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void bindElements(){
        youTubePlayer = findViewById(R.id.youtube_view);
        backUser = findViewById(R.id.userBackLearn);
    }

}
