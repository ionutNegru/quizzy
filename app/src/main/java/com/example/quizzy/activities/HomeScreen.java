package com.example.quizzy.activities;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import com.example.quizzy.R;
import com.example.quizzy.fragments.EditProfileFragment;
import com.example.quizzy.fragments.LearningFragment;
import com.example.quizzy.fragments.ProfileFragment;
import com.example.quizzy.fragments.SinglePlayerFragment;
import com.example.quizzy.fragments.ViewProfileCardFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.Map;

public class HomeScreen extends AppCompatActivity implements EditProfileFragment.OnFragmentInteractionListener, ViewProfileCardFragment.OnFragmentInteractionListener,
        LearningFragment.OnFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener{


    public  BottomNavigationView bottomNavigationView;
    private ProgressBar progressBar;
    private View homeScreen;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseUser user;
    private ArrayList<String> categoriesNames = new ArrayList<>();
    private long[] noQuestionsCategories = new long[3];
    private long[] noQuestionsAns = new long[3];
    int index = 0;
    int index2 = 0;
    private long categoriesTotal = 0;
    private long categoriesAns = 0;

    public HomeScreen() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        homeScreen = findViewById(R.id.homeScreen);
        homeScreen.setEnabled(false);
        homeScreen.setVisibility(View.INVISIBLE);

        bottomNavigationView =  findViewById(R.id.bottomNavView_bar);
        setupBottomNavigationBar();

        progressBar = findViewById(R.id.progress_bar);

        firebaseFirestore = FirebaseFirestore.getInstance();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();


        firebaseFirestore.collection("Categories").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    index = 0;
                    for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                        Map<String, Object> data = queryDocumentSnapshot.getData();

                        categoriesNames.add((String) data.get("Name"));
                        noQuestionsCategories[index] = (long) data.get("noQuestions");
                        index++;
                    }
                    firebaseFirestore.collection("UserInfo").document(user.getUid())
                            .collection("Categories").addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            if (queryDocumentSnapshots != null) {
                                index2 = 0;
                                for (QueryDocumentSnapshot queryDocumentSnapshot : queryDocumentSnapshots) {
                                    Map<String, Object> data = queryDocumentSnapshot.getData();

                                    noQuestionsAns[index2] = (long)data.get("noQuestionsAnswered");
                                    categoriesAns = categoriesAns + (long)data.get("noQuestionsAnswered");
                                    categoriesTotal = categoriesTotal + (long)data.get("totalQuestionsAnswered");
                                    index2++;
                                }
                                progressBar.setVisibility(View.INVISIBLE);
                                homeScreen.setEnabled(true);
                                homeScreen.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putParcelable("avatar", null);
    }

    public void setupBottomNavigationBar(){
        bottomNavigationView.setEnabled(true);
        final Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        changeFragment(0);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.sp_navBar) {
                        MenuItem menuItem = menu.getItem(0);
                        menuItem.setChecked(true);

                        changeFragment(0);
                } else if (id == R.id.book_navBar) {
                        MenuItem menuItem = menu.getItem(1);
                        menuItem.setChecked(true);

                        changeFragment(1);
                } else if (id == R.id.profile_navBar) {
                        MenuItem menuItem = menu.getItem(2);
                        menuItem.setChecked(true);

                        changeFragment(2);
                }
                return false;
            }
        });
    }

    public void changeFragment(int menuPosition) {


        Fragment fragment = new Fragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (menuPosition == 0) {

            fragment = new SinglePlayerFragment();

        } else if (menuPosition == 1) {

            fragment = new LearningFragment();

        } else if (menuPosition == 2) {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("categoriesNames",categoriesNames);
            bundle.putLong("noQuestionsAnswered",categoriesAns);
            bundle.putLong("totalQuestionsAnswered",categoriesTotal);
            bundle.putLongArray("noQuestionsCategories", noQuestionsCategories);
            bundle.putLongArray("noQuestionsAns", noQuestionsAns);

            fragment = new ProfileFragment();
            fragment.setArguments(bundle);
        }

        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }
}
